﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public float claculateValue(float x,float a,float b,float c)
        {
            if (x<=c)
            {
                return 1;
            }
            if (x>c)
            {
                double dIn = Math.Pow(1 + Math.Pow(a * (x - c), b),-1);
                float dOut = Convert.ToSingle(dIn);
                return dOut;
            }
            return 0;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series["Series1"].Points.Clear();
            chart1.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            float start = float.Parse(startVal.Text);
            float end = float.Parse(endVal.Text);
            float a = float.Parse(txta.Text);
            float b = float.Parse(txtb.Text);
            float c = float.Parse(txtc.Text);
            float y;
            for(float x = start; x <= end; x += 0.5f)
            {
                y = claculateValue(x, a, b, c);

                chart1.Series["Series1"].Points.AddXY(x,y);
                //chart1.Series["Series1"].Points.AddX(x);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
