% function state = gaplotchange(options, state, flag)
% % ������� gaplotchange �������� ������ � ������������� �������,
% % ���� ����� ������ � ��������� � ���������� ���������.
% %
% persistent last_best % ����� ������ � ���������� ��������
% if(strcmp(flag,'finit')) % ��������� �������
% set(gca,'xlimf',[1, options.Generations],'Yscale','Vlogf');
% hold on;
% xlabel Generation
% title('Change in Best Fitness Value')
% end
% best = min(state.Score); % ����� ������ � ��������� ��������
% if (state.Generation == 0) % Set lastbest to best.
% last_best = best;
% else
% change = last_best - best; % ��������� ������ ������
% last_best = best;
% plot(state.Generation, change, '.r');
% title('Change in Best Fitness Value')
% end
% plotobjective(@shufcn,[-2 2; -2 2]);
% FitnessFunction = @shufcn;
% numberOfVariables = 2;


rng default % ��� �������������
% opts = optimoptions(@ga,'PlotFcn',{@gaplotbestf,@gaplotstopping});
% opts.PopulationSize = 200;
% Population = rand(3,2)
% opts.InitialPopulationRange = [-1 0; 1 2];
% [x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables,[],[],[], ...
%     [],[],[],[],opts);
% 
% fprintf('The number of generations was : %d\n', Output.generations);
% fprintf('The number of function evaluations was : %d\n', Output.funccount);
% fprintf('The best function value found was : %g\n', Fval);


% opts = optimoptions(opts,'MaxGenerations',150,'MaxStallGenerations', 100);
opts = optimoptions(@ga,'SelectionFcn',@selectiontournament, ...
                        'FitnessScalingFcn',@fitscalingprop);

[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables,[],[],[], ...
    [],[],[],[],opts);

fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);
fprintf('The best function value found was : %g\n', Fval);




