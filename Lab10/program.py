
from threading import Thread
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import random
import math
import struct

def generate(n,a=5,b=1000):
  return np.random.randint(a,b,(n,n))

class ACO:
  class Muraha(Thread):
    def __init__(self,init_location,possible_locations,pheromone_map,distance_callback,alpha,beta,first_pass=False):
      Thread.__init__(self)
      self.init_location = init_location
      self.possible_locations = possible_locations
      self.route = []
      self.distance_traveled = 0.0
      self.location = init_location
      self.pheromone_map = pheromone_map
      self.distance_callback = distance_callback
      self.alpha = alpha
      self.beta = beta
      self.first_pass = first_pass
      self.update_route(init_location)
      self.tour_complete = False

    def run(self):
      while self.possible_locations:
        nxt = self.pick_path()
        self.traverse(self.location,nxt)
      self.possible_locations.append(self.init_location)
      self.tour_complete = True

    def pick_path(self):
      if self.first_pass:
        rnd = random.choice(self.possible_locations)
        while rnd == self.init_location and len(self.possible_locations) > 1:
          rnd = random.choice(self.possible_locations)
        return rnd
      attractivness = dict()
      sum_total = 0.0
      for possible_next_location in self.possible_locations:
        pheromone_amount = float(self.pheromone_map[self.location][possible_next_location])
        distance = float(self.distance_callback(self.location,possible_next_location))
        attractivness[possible_next_location] = pow(pheromone_amount,self.alpha) * pow(1/distance,self.beta)
        sum_total += attractivness[possible_next_location]
      if sum_total == 0.0:
        def next_up(x):
          if math.isnan(x) or (math.isinf(x) and x>0):
            return x
          if x == 0.0:
            x = 0.0
          n = struct.unpack('<q',struct.pack('<d',x))[0]
          if n>=0:
            n += 1
          else:
            n -= 1
          return struct.unpack('<d',struct.pack('<q',n))[0]

        for key in attractivness:
          attractivness[key] = next_up(attractivness[key])
        sum_total = next_up(sum_total)
      toss = random.random()

      cummulative = 0
      for possible_next_location in attractivness:
        weight = (attractivness[possible_next_location] / sum_total)
        if toss <= weight + cummulative:
          return possible_next_location
        cummulative += weight
      #return self.init_location

    def traverse(self,start,end):
      self.update_route(end)
      self.update_distance_traveled(start,end)
      self.location = end

    def update_route(self,new):
      self.route.append(new)
      self.possible_locations = list(self.possible_locations)
      self.possible_locations.remove(new)

    def update_distance_traveled(self,start,end):
      self.distance_traveled += float(self.distance_callback(start,end))

    def get_route(self):
      if self.tour_complete:
        return self.route
      return None

    def get_distance_traveled(self):
      if self.tour_complete:
        return self.distance_traveled
      return None
def __init__(self,nodes_num,distance_matrix,start,muraha_count,alpha,beta,pheromone_evapolation_coefficient,pheromone_constmuraha,iterations):
  self.nodes = list (range (nodes_num))
  self.nodes_num = nodes_num
  self.distance_matrix = distance_matrix
  self.pheromone_map = self.init_matrix(nodes_num)
  self.muraha_updated_pheromone_map = self.init_matrix(nodes_num)
  self.start = 0 if start is None else start
  self.muraha_count = muraha_count
  self.alpha = float (alpha)
  self.beta = float (beta)
  self.pheromone_evaporation_coefficient = float(pheromone_evaporation_coefficient)
  self.pheromone_constmuraha = float(pheromone_constmuraha)
  self.iterations = iterations
  self.first_pass = True
  self.murahas = self._init_murahae(self.start)
  self.shortest_distance = None
  self.shortest_path_seen = None

def get_distance(self, start, end):
  return self.distance_matrix[start][end]

def init_matrix(self, size, value=0.0):
  ret = []
  for row in range (size):
    ret.append([float (value) for _ in range(size)])
  return ret

def _init_murahas(self, start):
  if self.first_pass:
    return [self.muraha(start, self.nodes, self.pheromone_map, self.get_distance,
                    self.alpha, self.beta, first_pass=True) for _ in range (self.muraha_count)]
  for muraha in self.murahas:
    muraha.__init__(start, self.nodes, self.pheromone_map, self.get_distance, self.alpha, self.beta)

def update_pheromone_map(self):
  for start in range(len(self.pheromone_map)):
    for end in range(len(self.pheromone_map)):
      self.pheromone_map[start][end] *= (1 - self.pheromone_evapolation_coefficient)
      self.pheromone_map[start][end] += self.muraha_updated_pheromone_map[start][end]

def populate_muraha_updated_pheromone_map(self, muraha):
  route = muraha.get_route()
  for i in range(len(route) - 1):
    current_pheromone_value = float (self.muraha_updated_pheromone_map[route[i]] [route[i + 1]])
    new_pheromone_value = self.pheromone_constmuraha / muraha.get_distance_traveled()
    self.muraha_updated_pheromone_map[route[i]][route[i + 1]] = current_pheromone_value + new_pheromone_value
    self.muraha_updated_pheromone_map[route[i + 1]][route[i]] = current_pheromone_value + new_pheromone_value

def mainloop(self):
  for it in range(self.iterations):
    for muraha in self.murahas:
      muraha.start()
    for muraha in self.murahas:
      muraha.join()

    for muraha in self.murahas:
      self.populate_muraha_updated_pheromone_map (muraha)
      if not self.shortest_distance:
        self.shortest_distance = muraha.get_distance_traveled()

      if not self.shortest_path_seen:
        self.shortest_path_seen = muraha.get_route()
      if muraha.get_distance_traveled() < self.shortest_distance:
        self.shortest_distance = muraha.get_distance_traveled()
        self.shortest_path_seen = muraha.get_route()
    self.update_pheromone_map()

    if self.first_pass:
      self.first_pass = False

    self._init_murahas(self.start)

    self.muraha_updated_pheromone_map = self.init_matrix(self.nodes_num, value = 0)
    if (it +1 ) % 50 == 0:
      print('{0}/{1} Search....'. format(it+1,self.iterations))
  ret = []
  for ids in self.shortest_path_seen:
    ret.append(self.nodes[ids])

  return ret

START = None
muraha_COUNT = 100
ALPHA = 1.1
BETA = 1.1
PHER_EVAP_COEFF = 0.70
PHER_CONSTmuraha = 3000.0
ITERATIONS = 400

def main():
  distance_matrix = np.loadtxt(open("CityMatrix.txt",'rb'),delimiter=' ')

  colony = ACO(len(distance_matrix[0]),
              distance_matrix,
              START,
              muraha_COUNT,
              ALPHA,
              BETA,
              PHER_EVAP_COEFF,
              PHER_CONSTmuraha,
              ITERATIONS)
  answer = colony.mainloop()
  print(np.array(answer)+1)
  print(colony.shortest_distance)

  cities = {}
  with open("CityList.txt") as f:
    for line in f:
      (key,val) = line.split()
      cities[int(key)] = val
  dictkeys = {x:y for x,y in cities.items()}
  print("CITIES: ",dictkeys)
  dictkeys2 = {}
  for i in answer:
    k = cities.get(i+1)
    dictkeys2[i+1] = key

  path = list(dictkeys2.values())
  print("Way: ",path)

  N=26
  for i in range(len(answer)):
    answer[i]=answer[i]*10
  list_of_cities = ['Вінниця','Дніпропетровськ','Донецьк','Житомир','Запоріжжя','Івано-Франківськ','Київ','Кіровоград','Луганськ','Луцьк','Львів','Миколаїв','Одеса','Полтава','Рівне','Сімферополь','Суми','Тернопіль','Ужгород','Харків','Херсон','Хмельницький','Черкаси','Чернівці','Чернігів']
  ind = np.arrange(0,N,1)
  width = 0.35

  fig,ax = plt.sublopts(1,1)
  ax.scatter(ind,answer, marker='*',color='r')
  ax.set_ylable('CityName')
  ax.set_xlable('CityNumber')
  ax.set_title('Passed way')
  ax.set_ysticks(np.arrange(0,255,10))
  ax.set_ysticklabels(list_of_cities)
  ax.set_xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25])
  plt.plot(ind,answer,color="blue")
  plt.show()


if __name__ == '__main__':
  main()
