%�������� ������� ������� y=(x1^2-8)*cos(x2) 
%� ������ x1�[0,4] � x2�[0,4]. 
n=15; 
x1=0:4/(n-1):4; 
x2=0:4/(n-1):4; 
y=zeros(n,n); 
for j=1:n 
y(j,:)=(x1.^2-8)*cos(x2(j)); 
end 
surf(x1,x2,y) 
xlabel('x1') 
ylabel('x2') 
zlabel('y') 
title('Target');

% 2.2 

mam_fismat = readfis ('mam22.fis');

sug_fismat = mam2sug(mam_fismat);

subplot(2,2,1)
gensurf(mam_fismat,[1 2],1)
title('Mamdani system (Output 1)')
subplot(2,2,2)
gensurf(sug_fismat,[1 2],1)
title('Sugeno system (Output 1)')
subplot(2,2,3)
gensurf(mam_fismat,[1 2],2)
title('Mamdani system (Output 2)')
subplot(2,2,4)
gensurf(sug_fismat,[1 2],2)
title('Sugeno system (Output 2)')
%2.3
service = 0:.5:10;
tip = 0.15*ones(size(service));
plot(service,tip)
xlabel('Service')
ylabel('Tip')
ylim([0.05 0.25])


tip = (.20/10)*service+0.05;
plot(service,tip)
xlabel('Service')
ylabel('Tip')
ylim([0.05 0.25])


food = 0:.5:10;
[F,S] = meshgrid(food,service);
tip = (0.20/20).*(S+F)+0.05;
surf(S,F,tip)
xlabel('Service')
ylabel('Food')
zlabel('Tip')


servRatio = 0.8;
tip = servRatio*(0.20/10*S+0.05) + (1-servRatio)*(0.20/10*F+0.05);
surf(S,F,tip)
xlabel('Service')
ylabel('Food')
zlabel('Tip')

tip = zeros(size(service));
tip(service<3) = (0.10/3)*service(service<3)+0.05;
tip(service>=3 & service<7) = 0.15;
tip(service>=7 & service<=10) = ...
    (0.10/3)*(service(service>=7 & service<=10)-7)+0.15;
plot(service,tip)
xlabel('Service')
ylabel('Tip')
ylim([0.05 0.25])


servRatio = 0.8;
tip = zeros(size(S));
tip(S<3) = ((0.10/3)*S(S<3)+0.05)*servRatio + ...
    (1-servRatio)*(0.20/10*F(S<3)+0.05);
tip(S>=3 & S<7) = (0.15)*servRatio + ...
    (1-servRatio)*(0.20/10*F(S>=3 & S<7)+0.05);
tip(S>=7 & S<=10) = ((0.10/3)*(S(S>=7 & S<=10)-7)+0.15)*servRatio + ...
    (1-servRatio)*(0.20/10*F(S>=7 & S<=10)+0.05);
surf(S,F,tip)
xlabel('Service')
ylabel('Food')
zlabel('Tip')


gensurf(readfis('tipper'))


lowTip = 0.05;
averTip = 0.15;
highTip = 0.25;
tipRange = highTip-lowTip;
badService = 0;
okayService = 3; 
goodService = 7;
greatService = 10;
serviceRange = greatService-badService;
badFood = 0;
greatFood = 10;
foodRange = greatFood-badFood;
if service<okayService
    tip = (((averTip-lowTip)/(okayService-badService)) ...
        *service+lowTip)*servRatio + ...
        (1-servRatio)*(tipRange/foodRange*food+lowTip);
elseif service<goodService
    tip = averTip*servRatio + (1-servRatio)* ...
        (tipRange/foodRange*food+lowTip);
else
    tip = (((highTip-averTip)/ ...
        (greatService-goodService))* ...
        (service-goodService)+averTip)*servRatio + ...
        (1-servRatio)*(tipRange/foodRange*food+lowTip);
end





