



function calculateTips(){
  var price = $("#payment").val();
	var service = $("select#service").children("option:selected").val();
	var food = $( "select#food").children("option:selected").val();
	var tip;
	
	var lowTip = 0.05;
	var averTip = 0.15;
	var highTip = 0.25;
	var tipRange = highTip-lowTip;
	var badService = 0;
	var okayService = 3; 
	var goodService = 7;
	var greatService = 10;
	var serviceRange = greatService-badService;
	var badFood = 0;
	var greatFood = 10;
	var foodRange = greatFood-badFood;

	if (service<okayService || food < greatFood){
		tip = (((averTip-lowTip)/(okayService-badService))*service+lowTip)*serviceRange +(1-serviceRange)*(tipRange/foodRange*food+lowTip);
	}
	else if (service<=goodService) {
		tip = averTip*serviceRange + (1-serviceRange)*(tipRange/foodRange*food+lowTip);
	}
	else {
		tip = (((highTip-averTip)/ (greatService-goodService))* (service-goodService)+averTip)*serviceRange + (1-serviceRange)*(tipRange/foodRange*food+lowTip);	
	}
	
	
	$( "#tips" ).text("TIPS:"+(tip*price).toFixed(2));
}

calculateTips();